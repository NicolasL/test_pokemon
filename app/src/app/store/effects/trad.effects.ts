import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { map, mergeMap, catchError, exhaustMap } from 'rxjs/operators';
import { Filter } from 'src/app/classes/filter.class';
import { Traduction } from 'src/app/classes/traduction.class';
import { FilterService } from 'src/app/services/filter.service';
import { TradService } from 'src/app/services/trad.service';
import * as TradAction from '../actions/trad.action';
@Injectable()
export class TradEffects {

    constructor(
        private tradService: TradService,
        private actions$: Actions,
    ) {}
    
  loadFilter$ = createEffect(() => this.actions$.pipe(
    ofType(TradAction.loadTrad),
    exhaustMap(action =>  this.tradService.getTrad(action.lang)
      .pipe(
        map((trad: Traduction) => ({ type: '[App] Load Trad Success', trad })),
        catchError(() => EMPTY)
      ))
    )
  );

}