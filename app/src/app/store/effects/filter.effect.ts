import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { Filter } from 'src/app/classes/filter.class';
import { FilterService } from 'src/app/services/filter.service';
 
@Injectable()
export class FilterEffects {
 
  loadFilter$ = createEffect(() => this.actions$.pipe(
    ofType('[Cards Component] Load Filter'),
    mergeMap(() => this.filterService.getFilter()
      .pipe(
        map((filter: Filter) => ({ type: '[Cards Component] Filter Loaded Success', filter })),
        catchError(() => EMPTY)
      ))
    )
  );
 
  constructor(
    private actions$: Actions,
    private filterService: FilterService
  ) {}
}