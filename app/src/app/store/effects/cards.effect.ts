import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { defer, EMPTY } from 'rxjs';
import { map, mergeMap, catchError, switchMap } from 'rxjs/operators';
import { CustomCard } from 'src/app/classes/custom-card.class';
import { CardsService } from 'src/app/services/cards.service';
 
@Injectable()
export class CardsEffects {

  /**
   * liste de toutes les cartes
   */
  private cards: Array<CustomCard> = []
  /**
   * liste des 20 premieres cartes modifiées
   */
  private fastCards: Array<CustomCard> = []

  constructor(
    private actions$: Actions,
    private cardsService: CardsService,
    private sanitizer: DomSanitizer
  ) {}
 
  /**
   * Chagement des 20 premieres cartes
   */
  loadCardsFast$ = createEffect(() => this.actions$.pipe(
    ofType('[Cards Component] Load Cards Fast'),
    mergeMap(() => this.cardsService.getCards()
      .pipe(
        switchMap((cards: Array<CustomCard>) => {
          this.cards = [...cards]
          const partialCards = this.cards.slice(0,20)
          partialCards.forEach((card: CustomCard) => card.shoppingCart = 0);
          const observable$ = defer(async () => await this.loadImage(partialCards, 0));
          return observable$.pipe(
            map((cards: Array<CustomCard>) => {
              this.fastCards = cards
              return ({ type: '[Cards Component] Cards Loaded Fast Success', cards })
            })
          )
        })
      )),
      catchError(() => EMPTY),
    )
  );

  /**
   * Chagement de toutes les cartes sauf les 20 premieres
   */
  loadCards$ = createEffect(() => this.actions$.pipe(
    ofType('[Cards Component] Load Cards'),
      mergeMap(() => {
        const partialCards = this.cards.slice(20, this.cards.length);
        partialCards.forEach((card: CustomCard) => card.shoppingCart = 0);
          const observable$ = defer(async () => await this.loadImage(partialCards, 0));
          return observable$.pipe(
            map((cards: Array<CustomCard>) => {
              cards = [...this.fastCards, ...cards]
              return ({ type: '[Cards Component] Cards Loaded Success', cards })
            }
          )
        )
      }),
      catchError(() => EMPTY),
    )
  );

  /**
   * affectation des images transformées à la propriété images.image de l'objet CustomCard
   * @param cards Array<CustomCard>
   * @param counter number
   * @returns Promise<Array<CustomCard>>
   */
  private async loadImage(cards: Array<CustomCard>, counter: number): Promise<Array<CustomCard>> {
    cards[counter].images.image = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(await this.loadXHR(cards[counter].images.small)));
    counter++;
    if (counter < cards.length) {
      return await this.loadImage(cards, counter)
    } else {
      return cards
    }
  }
  

  /**
   * Transformation des urls des images en blob
   * @param url string
   * @returns Promise<Blob>
   */
  private loadXHR(url: string): Promise<Blob> {
    return new Promise(function(resolve, reject) {
        try {
            var xhr = new XMLHttpRequest();
            xhr.open("GET", url);
            xhr.responseType = "blob";
            xhr.onerror = function() {reject("Network error.")};
            xhr.onload = function() {
                if (xhr.status === 200) {resolve(xhr.response)}
                else {reject("Loading error:" + xhr.statusText)}
            };
            xhr.send();
        }
        catch(err) {reject(err.message)}
    });
  }

}