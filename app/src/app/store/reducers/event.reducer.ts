import { createReducer, on } from '@ngrx/store';
import { openFilter } from '../actions/event.actions';
 
export const initialState: {isOpen: boolean | null} = {isOpen: null};
 
const _eventReducer = createReducer(
  initialState,
  on(openFilter, (state, props: {data: boolean | null}) => {
        return {
            ...state,
            isOpen: props.data
        }
    }),
);


 
export function eventReducer(state: any, action: any) {
  return _eventReducer(state, action);
}