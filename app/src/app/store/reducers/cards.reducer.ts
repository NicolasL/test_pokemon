import { createReducer, on } from '@ngrx/store';
import { CustomCard } from 'src/app/classes/custom-card.class';
import { addCard, loadCard, loadCardFast, loadCardFastSuccess, loadCardSuccess, removeCard } from '../actions/cards.actions';
 
export const initialState: Array<CustomCard> = [];
 
const _cardsReducer = createReducer(
  initialState,
  on(addCard, (state, props: {id: string}) => {
    let newState = new Array<CustomCard>()
    state.map((card: CustomCard) => {
        if(card.id === props.id) {
          let numberCardOnShop = JSON.parse(JSON.stringify(card.shoppingCart));
          numberCardOnShop++;
          newState = [...newState, {...card, shoppingCart: numberCardOnShop}]
        } else {
          newState = [...newState, {...card}]
        }
      }
    ) 
    return newState;  
  }),
  on(removeCard, (state, props: {id: string}) => {
    let newState = new Array<CustomCard>()
    state.map((card: CustomCard) => {
        if(card.id === props.id) {
          let numberCardOnShop = JSON.parse(JSON.stringify(card.shoppingCart));
          numberCardOnShop--;
          newState = [...newState, {...card, shoppingCart: numberCardOnShop}]
        } else {
          newState = [...newState, {...card}]
        }
      }
    ) 
    return newState; 
  }),
  on(loadCard, (state) => state),
  on(loadCardSuccess, (state, payload: any) => {
    return [...payload.cards]
  }),
  on(loadCardFast, (state) => state),
  on(loadCardFastSuccess, (state, payload: any) => {
    return [...payload.cards]
  })
);
 
export function cardsReducer(state: any, action: any) {
  return _cardsReducer(state, action);
}