import { createReducer, on } from '@ngrx/store';
import { Filter } from 'src/app/classes/filter.class';
import { loadFilter, loadFilterSuccess, updateFilter } from '../actions/filter.actions';
 
export const initialState: Filter = new Filter();
 
const _filterReducer = createReducer(
  initialState,
  on(loadFilter, (state) => state),
  on(loadFilterSuccess, (state, payload: any) => {
    return payload.filter;
  }),
  on(updateFilter, (state, filter: Filter) => state = filter),
);


 
export function filterReducer(state: any, action: any) {
  return _filterReducer(state, action);
}