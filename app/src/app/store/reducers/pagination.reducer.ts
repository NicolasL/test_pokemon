import { createReducer, on } from '@ngrx/store';
import { getPagination, updatePagination } from '../actions/pagination.action';
 
export const initialState: number = 0;
 
const _paginationReducer = createReducer(
  initialState,
  on(getPagination, (state) => state),
  on(updatePagination, (state, payload: {page: number}) => {
    state = payload.page
    return state;
  }),
);


export function paginationReducer(state: any, action: any) {
  return _paginationReducer(state, action);
}