import { createReducer, on } from '@ngrx/store';
import { Traduction } from 'src/app/classes/traduction.class';
import { openFilter } from '../actions/event.actions';
import { loadTrad, loadTradSuccess } from '../actions/trad.action';
 
export const initialState: Traduction = {} as Traduction ;
 
const _traductionReducer = createReducer(
    initialState,
    on(loadTrad, (state) => state),
    on(loadTradSuccess, (state, payload: any) => {
        return {...payload.trad}
    })
);

export function traductionReducer(state: any, action: any) {
  return _traductionReducer(state, action);
}
