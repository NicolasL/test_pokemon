import { CustomCard } from '../classes/custom-card.class';
import { Filter } from '../classes/filter.class';
import { Traduction } from '../classes/traduction.class';

export interface AppState {
  cards: Array<CustomCard>;
  filter: Filter;
  pagination: number;
  isOpen: boolean | null;
  trad: Traduction
}