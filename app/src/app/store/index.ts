import { createSelector } from "@ngrx/store";
import { CustomCard } from "../classes/custom-card.class";
import { Filter } from "../classes/filter.class";
import { Traduction } from "../classes/traduction.class";
import { AppState } from "./app.state";

export const selectAllCards = (state: AppState) => state.cards;
export const selectFilter = (state: AppState) => state.filter;
export const selectPagination = (state: AppState) => state.pagination;
export const selectTrad = (state: AppState) => state.trad;

/**
 * Retourne la traduction
 */
export const SelectedTrad  = createSelector(
    selectTrad,
    (selectTrad: Traduction) => {
        return selectTrad;
    }
)

/**
 * Retourne les cartes qui sont dans le panier sans prendre en compte les doublons
 */
export const cardInShoppingCart = createSelector(
    selectAllCards,
    (selectAllCards: Array<CustomCard>) => {
        const cards = new Array<CustomCard>()
        selectAllCards.forEach((card: CustomCard) => {
            if (card.shoppingCart > 0) {
                cards.push(card)
            }
        })
        return cards
    }  
)

/**
 * Retourne les cartes qui sont dans le panier en prenant en compte les doublons
 */
export const numberCardInShoppingCart = createSelector(
    selectAllCards,
    (selectAllCards: Array<CustomCard>) => {
        const cards = new Array<CustomCard>()
        selectAllCards.forEach((card: CustomCard) => {
            if (card.shoppingCart > 0) {
                for (let i = 0; i < card.shoppingCart; i++) {
                    cards.push(card)
                }
            }
        })
        return cards
    }  
)

/**
 * Retourne les filtres
 */
export const selectedFilter = createSelector(
    selectFilter,
    (selectFilter: Filter) => {
        return selectFilter
    }
)

/**
 * Retourne la page en cours
 */
export const selectedPagination = createSelector(
    selectPagination,
    (selectPagination: number) => {
        return selectPagination
    }
)

/**
 * Retourne les cartes
 */
export const allCards = createSelector(
    selectAllCards,
    (selectAllCards: Array<CustomCard>) => {
        return selectAllCards
    }
)

/**
 * Retourne les cartes en fonction du filtre
 */
export const selectFilteredCards = createSelector(
    selectAllCards,
    selectFilter,
    selectPagination,
    (selectAllCards: Array<CustomCard>, selectFilter: Filter, selectPagination: number) => {
        let cards = new Array<CustomCard>()
        if (selectAllCards && selectFilter) {
            if (selectFilter.rarity) {
                cards = selectAllCards.filter((card: CustomCard) => card.rarity === selectFilter.rarity)
            } else {
                cards = selectAllCards
            }
            if (selectFilter.showShopping) {
                cards = cards.filter((card: CustomCard) => card.shoppingCart > 0)
            }
            cards = cards.slice(selectPagination * selectFilter.nbrByPage, (selectPagination + 1) * selectFilter.nbrByPage)
            return cards
        } else {
            return selectAllCards;
        }
    }
  );
