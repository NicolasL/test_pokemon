import { createAction, props } from '@ngrx/store';

export const getPagination = createAction('[Pagination Component] Get Pagination')
export const updatePagination = createAction('[Pagination Component] Update Pagination', props<{page: number}>())
