import { createAction, props } from '@ngrx/store';

export const loadCard = createAction('[Cards Component] Load Cards')
export const loadCardSuccess = createAction('[Cards Component] Cards Loaded Success')
export const loadCardFast = createAction('[Cards Component] Load Cards Fast')
export const loadCardFastSuccess = createAction('[Cards Component] Cards Loaded Fast Success')
export const addCard = createAction('[Cards Component] AddCard',props<{id: string}>());
export const removeCard = createAction('[ShoppingCart Component] removeCard',props<{id: string}>());
