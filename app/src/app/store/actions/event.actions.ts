import { createAction, props } from "@ngrx/store";

export const openFilter = createAction('[banner Component] Open Filter', props<{data: boolean | null}>())