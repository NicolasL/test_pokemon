import { createAction, props } from '@ngrx/store';

export const loadTrad = createAction('[App] Load Trad', props<{lang: string}>())
export const loadTradSuccess = createAction('[App] Load Trad Success')
