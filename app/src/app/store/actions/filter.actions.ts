import { createAction, props } from '@ngrx/store';
import { Filter } from 'src/app/classes/filter.class';

export const loadFilter = createAction('[Cards Component] Load Filter')
export const loadFilterSuccess = createAction('[Cards Component] Filter Loaded Success')
export const updateFilter = createAction('[Filter Component] Update Filter', props<Filter>())
