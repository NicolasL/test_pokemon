import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { combineLatest, Subscription } from 'rxjs';
import { allCards, selectFilter } from 'src/app/store';
import { AppState } from 'src/app/store/app.state';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit, OnDestroy {

  paginationSubscription: Subscription | undefined
  nbrPageSubscription: Subscription | undefined

  /**
   * nombre de page
   */
  nombrePage: Array<number> = []
  /**
   * page courante
   */
  pagination: number = 1;
  /**
   * nombre d'éléments par pages
   */
  nbrByPage = 0
  
  constructor(
    private store: Store<AppState>
  ) { }

  ngOnInit(): void {
    // Récupere la page courante
    this.paginationSubscription = this.store.select<number>(state => state.pagination).subscribe(
      (pagination: number) => {
        this.pagination = pagination;
      }
    )

    this.store.dispatch({type: '[Pagination Component] Get Pagination'})

    // calacule le nombre de page en fonction du nombre d'éléments par pages
    this.nbrPageSubscription = combineLatest([this.store.select(selectFilter), this.store.select(allCards)]).subscribe(
      (res: any) => {
        this.nbrByPage = res[0].nbrByPage
        this.nombrePage = []
        const nbrPage = res[1].length / res[0].nbrByPage;
        for (let i = 0; i < nbrPage; i++) {
          this.nombrePage.push(i)
        }
      }
    )
  }

  ngOnDestroy() {
    if (this.nbrPageSubscription) {
      this.nbrPageSubscription.unsubscribe();
    }
    if (this.paginationSubscription) {
      this.paginationSubscription.unsubscribe();
    }
  }

  /**
   * Charge la nouvelle page
   * @param page number
   */
  changePage(page: number): void {
      this.store.dispatch({type: '[Pagination Component] Update Pagination', page})
  }
}
