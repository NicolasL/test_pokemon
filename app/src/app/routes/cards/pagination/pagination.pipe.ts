import { Pipe, PipeTransform } from '@angular/core';
import { CustomCard } from 'src/app/classes/custom-card.class';

@Pipe({
  name: 'paginationPipe',
})
export class PaginationPipe implements PipeTransform {
  transform(pages: Array<number>, currentPage: number) {
      if (pages.length > 3) {
          return pages.slice(currentPage, currentPage+3)
      } else {
          return pages
      }
  }
}

