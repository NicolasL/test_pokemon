import { AfterViewInit, Component, ElementRef, HostListener, Input, Renderer2, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { CustomCard } from 'src/app/classes/custom-card.class';
import { AppState } from 'src/app/store/app.state';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent {

  @Input() card: CustomCard =  {} as CustomCard

  @ViewChild('infoElement') infoElement: ElementRef | undefined

  constructor(
    private render: Renderer2,
    private store: Store<AppState>
  ) { }

  /**
   * Animation permattant de montrer les informations d'une carte
   */
  showInfo() {
    if (this.infoElement) {
      this.render.removeClass(this.infoElement.nativeElement, 'infoTransitionClose')
      this.render.addClass(this.infoElement.nativeElement, 'infoTransitionOpen')
    }
  }

  /**
   * Animation permattant de cacher les informations d'une carte
   */
  hideInfo() {
    if (this.infoElement) {
      this.render.removeClass(this.infoElement.nativeElement, 'infoTransitionOpen')
      this.render.addClass(this.infoElement.nativeElement, 'infoTransitionClose')
    }
  }

  /**
   * Ajoute une carte au panier
   * @param card CustomCard
   */
  addCard(card: CustomCard) {
    this.store.dispatch({type: '[Cards Component] AddCard', id: card.id})
  }

}
