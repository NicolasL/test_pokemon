import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardsComponent } from './cards.component';
import { CardComponent } from './card/card.component';
import { CardsRoutingModule } from './cards.routing.module';
import { FilterComponent } from './filter/filter.component';
import { FormsModule } from '@angular/forms';
import { FilterService } from '../../services/filter.service';
import { CardsService } from '../../services/cards.service';
import { PaginationComponent } from './pagination/pagination.component';
import { PaginationPipe } from './pagination/pagination.pipe';
import { TradDirective } from 'src/app/utities/traduction/trad.directive';
import { TradPipe } from 'src/app/utities/traduction/trad.pipe';
import { TradModule } from 'src/app/utities/traduction/trad.module';



@NgModule({
  declarations: [
    CardsComponent,
    CardComponent,
    FilterComponent,
    PaginationComponent,
    PaginationPipe,
  ],
  imports: [
    CommonModule,
    CardsRoutingModule,
    FormsModule,
    TradModule
  ],
  providers: [
    FilterService,
    CardsService
  ]
})
export class CardsModule { }
