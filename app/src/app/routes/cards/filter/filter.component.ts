import { Component, ElementRef, OnDestroy, OnInit, Renderer2, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { Filter } from 'src/app/classes/filter.class';
import { FilterService } from 'src/app/services/filter.service';
import { LoaderService } from 'src/app/services/loader.service';
import { selectedFilter } from 'src/app/store';
import { updateFilter } from 'src/app/store/actions/filter.actions';
import { AppState } from 'src/app/store/app.state';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit, OnDestroy{

  filterSubscription: Subscription | undefined
  /**
   * Elément html filter
   */
  @ViewChild('filter') filterEl: ElementRef | undefined;

  /**
   * Récupere l'objet filtre
   */
  filter$: Observable<Filter> = this.store.select(selectedFilter);
  /**
   * Récupérer la liste des raretés de cartes
   */
  rarity$: Observable<Array<string>> = this.filterService.getRarity();
  /**
   * true dés que la totalités des cartes sont chargées
   */
  isLoad$: Observable<boolean> = this.loaderService.isLoaded$

  /**
   * Nombre d'éléments par pages possibles
   */
  public elementsByPages: Array<Number> = [20, 50, 100, 150, 200, 250];

  constructor(
    private store: Store<AppState>,
    private filterService: FilterService,
    private render: Renderer2,
    private loaderService: LoaderService
  ){}

  ngOnInit() {
    // determine si le filtre est affiché ou non (mobile)
    this.filterSubscription = this.store.select<any>(state => state.isOpen).subscribe(
      (isOpen: {isOpen: boolean | null}) => {
        this.openFilter(isOpen.isOpen)
      }
    )
  }

  ngOnDestroy() {
    if (this.filterSubscription) {
      this.filterSubscription.unsubscribe();
    }
  }

  /**
   * Mise à jour du filtre
   * @param target event.target
   * @param key string
   * @param filter Filter
   * @param type string
   */
  changeFilter(target: any, key: string, filter: Filter, type: string) {
    const f = {...filter}
    switch (type) {
      case 'number':
        f[key] = Number(target.value);
        break;
      case 'string':
        f[key] = target.value;
        break;
      case 'boolean':
        f[key] = JSON.parse(target.value); 
    }
    this.store.dispatch(updateFilter(f))
    this.store.dispatch({type: '[Pagination Component] Update Pagination', page:0})
  }

  /**
   * Ajoute et enleve des classes permettant les animations d'ouvertures 
   * et fermetures du filtre en mode mobile
   * @param filterOpen boolean | null
   */
  openFilter(filterOpen: boolean | null) {
    if (this.filterEl) {
      if (filterOpen === true) {
        this.render.addClass(this.filterEl.nativeElement, 'filterTransitionOpen')
        this.render.removeClass(this.filterEl.nativeElement, 'filterTransitionClose')
      } else if (filterOpen === false){
        this.render.addClass(this.filterEl.nativeElement, 'filterTransitionClose')
        this.render.removeClass(this.filterEl.nativeElement, 'filterTransitionOpen')
      }
    }
  }
}
