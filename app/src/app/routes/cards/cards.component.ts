import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { CustomCard } from 'src/app/classes/custom-card.class';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app.state';
import { selectFilteredCards } from 'src/app/store';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent {

  /**
   * Liste des cartes filtrées
   */
  cards$: Observable<Array<CustomCard>> = this.store.select(selectFilteredCards)
  
  constructor(
    private store: Store<AppState>,
  ) { }
}
