import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { CustomCard } from 'src/app/classes/custom-card.class';
import { AppState } from 'src/app/store/app.state';

@Component({
  selector: 'app-shopping-cart-card',
  templateUrl: './shopping-cart-card.component.html',
  styleUrls: ['./shopping-cart-card.component.css']
})
export class ShoppingCartCardComponent {

  @Input() card: CustomCard = {} as CustomCard

  constructor(
    private store: Store<AppState>
  ) { }

  /**
   * Suppression d'une carte du panier
   * @param card CustomCard
   */
  removeCard(card: CustomCard) {
    this.store.dispatch({type: '[ShoppingCart Component] removeCard', id: card.id})
  }



}
