import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShoppingCartRoutingModule } from './shopping-cart.routing.module';
import { ShoppingCartComponent } from './shopping-cart.component';
import { ShoppingCartCardComponent } from './shopping-cart-card/shopping-cart-card.component';
import { TradModule } from 'src/app/utities/traduction/trad.module';

@NgModule({
  declarations: [
    ShoppingCartComponent,
    ShoppingCartCardComponent
  ],
  imports: [
    CommonModule,
    ShoppingCartRoutingModule,
    TradModule
  ]
})
export class ShoppingCartModule { }
