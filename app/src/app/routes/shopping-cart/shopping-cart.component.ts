import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { CustomCard } from 'src/app/classes/custom-card.class';
import { cardInShoppingCart } from 'src/app/store';
import { AppState } from 'src/app/store/app.state';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent {

  /**
   * Cartes présentent dans le panier
   */
  cards$: Observable<Array<CustomCard>> = this.store.select(cardInShoppingCart)

  constructor(
    private store: Store<AppState>
  ) { }

}
