import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { LoaderService } from 'src/app/services/loader.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent  {

  isLoad$: Observable<boolean> = this.loaderService.isLoadedFast$;

  constructor(
    private loaderService: LoaderService
  ) { }

}
