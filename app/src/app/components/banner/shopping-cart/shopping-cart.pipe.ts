import { Pipe, PipeTransform } from '@angular/core';
import { CustomCard } from 'src/app/classes/custom-card.class';

@Pipe({
  name: 'pricePipe',
})
export class PricePipe implements PipeTransform {
  transform(cards: Array<CustomCard>) {
    let price = 0;
    cards.forEach((card: CustomCard) =>{
        if (card.tcgplayer?.prices?.holofoil?.market) {
            price += card.tcgplayer?.prices?.holofoil?.market
        }
    })
    return `${Math.round((price) * 100) / 100}€`;
  }
}

