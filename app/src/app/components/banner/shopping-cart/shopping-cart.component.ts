import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { CustomCard } from 'src/app/classes/custom-card.class';
import { numberCardInShoppingCart } from 'src/app/store';
import { AppState } from 'src/app/store/app.state';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {

  cards: Array<CustomCard> = []

  constructor(
    private store: Store<AppState>
  ) { }

  ngOnInit(){
    this.store.select(numberCardInShoppingCart).subscribe(
      (cards: Array<CustomCard>) => {
        this.cards = cards
      }
    )
  }
}
