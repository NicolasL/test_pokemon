import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { AppState } from 'src/app/store/app.state';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit, OnDestroy  {

  /**
   * Evenement lors du resize de la fenetre, permet de gérer 
   * l'affichage du filtre en fonction de la taille de l'ecran
   * @param e any
   */
  @HostListener('window:resize', ['$event']) resize(e: any) {
    if (e.target.innerWidth < 649 ) {
      if (this.isMobile === false) {
        this.isOpen = false
        this.store.dispatch({type: '[banner Component] Open Filter', data: this.isOpen})
      }
      this.isMobile = true
    } else {
      if (this.isMobile === true) {
        this.store.dispatch({type: '[banner Component] Open Filter', data: null})
      }
      this.isMobile = false
    }
  }


  private routerSubscription: Subscription | undefined;
  /**
   * Détermine si on est en vue mobile
   */
  private isMobile: boolean = window.innerWidth < 649 ? true : false;
  /**
   * Determine si le filtre est ouvert ou fermé
   */
  public isOpen: boolean = true;
  /**
   * Titre de la page
   */
  public title: string = '';
  /**
   * Permet d'afficher le boutton filtre uniquement sur certaines pages
   */
  public display: boolean = false

  constructor(
    private store: Store<AppState>,
    private router: Router
  ) {}

  ngOnInit(): void {
    // Affiche le titre en fonction des routes
    this.routerSubscription = this.router.events.pipe(filter(e => e instanceof NavigationEnd)).subscribe(
      (route: any) => {
        switch (route.url) {
          case '/':
          case '/cards':
            this.title = 'cardsList';
            this.display = true;
            break;
          case '/shopping':
            this.title = 'Shoping-cart';
            this.display = false;

        }
      }
    )    
  }

  ngOnDestroy() {
    if (this.routerSubscription) {
      this.routerSubscription.unsubscribe();
    }
  }

  /**
   * Determine si le filtre est ouvert ou fermé
   */
  openFilter() {
    this.isOpen = !this.isOpen;
    this.store.dispatch({type: '[banner Component] Open Filter', data: this.isOpen})
    
  }

}
