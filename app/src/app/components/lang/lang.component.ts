import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app.state';

@Component({
  selector: 'app-lang',
  templateUrl: './lang.component.html',
  styleUrls: ['./lang.component.css']
})
export class LangComponent {

  @ViewChild('lang') lang: ElementRef | undefined

  imageSelected: string = 'assets/images/fr.png'
  languages = [
    {country: 'fr', src:"assets/images/fr.png"},
    {country: 'en', src:"assets/images/en.png"},
  ]

  public isShowList = false;

  constructor(
    private store: Store<AppState>
  ) { }

  selectLang(lang: any) {
    this.imageSelected = lang.src;
    this.store.dispatch({ type: '[App] Load Trad', lang: lang.country})
    this.isShowList = false;
  }

  closeList() {
    this.isShowList = false;
  }

  showList() {
    if (!this.isShowList) {
      this.isShowList = true;
      if (this.lang) {
          this.lang.nativeElement.focus()
      }
    }
  }

}
