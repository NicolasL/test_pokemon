import { SafeUrl } from '@angular/platform-browser';
import { Card, CardImage, } from 'pokemon-tcg-sdk-typescript/dist/sdk';

/**
 * Ajout de la propriété image à l'interface CardImage
 */
export interface ICardImageCustom extends CardImage {
    image: SafeUrl
}

/**
 * Override de la propriété images par ICardImageCustom et ajout de la propriété shoppingCart
 */
export interface CustomCard extends Omit<Card, 'images'> {
    images: ICardImageCustom;
    shoppingCart: number;
}


