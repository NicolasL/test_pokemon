import { Rarity } from "pokemon-tcg-sdk-typescript/dist/sdk";
/**
 * [key: string]: any est une mauvaise pratique 
 * Dans le cas présent la classe est indéxé pour pouvoir 
 * modifier des propriétés de maniere dynamique.
 */
export class Filter {
    [key: string]: any
    nbrByPage: number = 0;
    rarity: Rarity | null = null;
    showShopping: boolean = false;
}