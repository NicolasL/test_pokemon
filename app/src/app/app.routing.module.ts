import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
    {path: '', redirectTo: 'cards', pathMatch: 'full'},
    {path: 'cards', loadChildren: () => import('./routes/cards/cards.module').then(m => m.CardsModule)},
    {path: 'shopping', loadChildren: () => import('./routes/shopping-cart/shopping-cart.module').then(m => m.ShoppingCartModule)}
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule {}