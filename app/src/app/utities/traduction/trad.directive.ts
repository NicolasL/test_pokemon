import { Directive, ElementRef, Input, Renderer2, TemplateRef, ViewContainerRef } from '@angular/core';
import { Store } from '@ngrx/store';
import { Traduction } from '../../classes/traduction.class';
import { SelectedTrad } from '../../store';
import { AppState } from '../../store/app.state';
 
@Directive({
  selector: '[translate]'
})
export class TradDirective{
 
    constructor(
        private store: Store<AppState>,
        private element: ElementRef,
      ){ }
     
      /**
       * Ajoute le texte traduit à l'élément parent
       */
      @Input()
      set translate(value:string){
        this.store.select(SelectedTrad).subscribe(
        (trad: Traduction) => {
            this.element.nativeElement.textContent = '';
            if (trad && value) {
                this.element.nativeElement.textContent = trad[value];         
            } 
        })
      }


 
}