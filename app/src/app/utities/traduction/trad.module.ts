import { NgModule } from "@angular/core";
import { TradDirective } from "./trad.directive";
import { TradPipe } from "./trad.pipe";

@NgModule({
    declarations: [
        TradDirective,
        TradPipe
    ],
    exports: [
        TradDirective,
        TradPipe
    ]
})
export class TradModule {}