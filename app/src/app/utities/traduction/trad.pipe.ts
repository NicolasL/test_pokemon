import { Pipe, PipeTransform } from '@angular/core';
import { Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { Traduction } from '../../classes/traduction.class';
import { SelectedTrad } from '../../store';
import { AppState } from '../../store/app.state';

@Pipe({
  name: 'translate',
})
export class TradPipe implements PipeTransform {

    constructor(
        private store: Store<AppState>
    ){}

    /**
     * Ajoute la traduction
     * @param value string
     * @returns string
     */
    transform(value: string) {
        return this.store.select(SelectedTrad).pipe(
            map((trad: Traduction) => {
                if (!trad || !value) {
                    return ''
                } else {
                    return trad[value]
                }
            })
        )
    }
}
