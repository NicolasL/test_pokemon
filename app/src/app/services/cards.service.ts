import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CustomCard } from 'src/app/classes/custom-card.class';


@Injectable({
  providedIn: 'root'
})
export class CardsService {


  constructor(
    private http: HttpClient,
  ) { }

  /**
   * Retourne la liste des cartes
   * @returns Observable<Array<CustomCard>>
   */
  getCards(): Observable<Array<CustomCard>> {
    return this.http.get<{data: Array<CustomCard>}>('https://api.pokemontcg.io/v2/cards').pipe(
      map((data: {data: Array<CustomCard>}) => {
        return data.data
      })
    )
  }
}

