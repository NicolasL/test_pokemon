import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  /**
   * Retourne un boolean permettant d'afficher le loader apres le chargement des premieres cartes
   */
  isLoadedFast$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  /**
   * Retourne un boolean permettant d'afficher le loader apres le chargement de toutes les cartes
   */
  isLoaded$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
}
