import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Traduction } from '../classes/traduction.class';

@Injectable({
  providedIn: 'root'
})
export class TradService {

  constructor(
    private http: HttpClient
  ) { }

  /**
   * Retourne le fichier de traductio en fonction de la langue
   * @param lang string
   * @returns Observable<Traduction>
   */
  getTrad(lang: string): Observable<Traduction> {
    return this.http.get<Traduction>(`assets/trad/${lang}.json`)
  }


}
