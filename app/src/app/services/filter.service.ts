import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Filter } from "../classes/filter.class";

@Injectable({
    providedIn: 'root'
})

export class FilterService {

    constructor(
        private http: HttpClient
    ){}

    /**
     * Retourne les filtres
     * @returns Observable<Filter>
     */
    getFilter(): Observable<Filter> {
        return this.http.get<Filter>('assets/fakeApi/filter.json')
    }

    /**
     * Retourne la liste des raretés de cartes
     * @returns Observable<Array<string>>
     */
    getRarity(): Observable<Array<string>> {
        return this.http.get<{data: Array<string>}>('https://api.pokemontcg.io/v2/rarities').pipe(
            map((data: {data: Array<string>}) => {
                return data.data
            })
        )
    }
}