import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { CustomCard } from './classes/custom-card.class';
import { Filter } from './classes/filter.class';
import { Traduction } from './classes/traduction.class';
import { LoaderService } from './services/loader.service';
import { AppState } from './store/app.state';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  private cardsSubscription: Subscription | undefined
  private langSubscription: Subscription | undefined
  private filterSubscription: Subscription | undefined

  constructor(
    private store: Store<AppState>,
    private loaderService: LoaderService,
  ){}

  ngOnInit() {
    this.cardsSubscription = this.store.select<Array<CustomCard>>(state => state.cards).subscribe(
      (cards: Array<CustomCard>) => {
        if(cards.length === 20) {
          this.loaderService.isLoadedFast$.next(true)
          this.store.dispatch({ type: '[Cards Component] Load Cards'})
        } else if (cards.length > 20) {
          this.loaderService.isLoaded$.next(true)
        }
      }
    )
    this.filterSubscription = this.store.select<Filter>(state => state.filter).subscribe()
    this.langSubscription = this.store.select<Traduction>(state => state.trad).subscribe()

    // Chargement de la langue
    this.store.dispatch({ type: '[App] Load Trad', lang: 'fr'})
    // Chargement des images
    this.store.dispatch({ type: '[Cards Component] Load Cards Fast'})
    // Chargement des filtres
    this.store.dispatch({ type: '[Cards Component] Load Filter'})
  }

  ngOnDestroy() {
    if (this.cardsSubscription) {
      this.cardsSubscription.unsubscribe();
    }
    if (this.filterSubscription) {
      this.filterSubscription.unsubscribe();
    }
    if (this.langSubscription) {
      this.langSubscription.unsubscribe();
    }
  }
}
