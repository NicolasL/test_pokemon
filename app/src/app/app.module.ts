import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.module';
import { LoaderModule } from './components/loader/loader.module';
import { CardsEffects } from './store/effects/cards.effect';
import { FilterEffects } from './store/effects/filter.effect';
import { TradEffects } from './store/effects/trad.effects';
import { cardsReducer } from './store/reducers/cards.reducer';
import { eventReducer } from './store/reducers/event.reducer';
import { filterReducer } from './store/reducers/filter.reducer';
import { paginationReducer } from './store/reducers/pagination.reducer';
import { traductionReducer } from './store/reducers/trad.reducer';
import { BannerComponent } from './components/banner/banner.component';
import { PricePipe } from './components/banner/shopping-cart/shopping-cart.pipe';
import { ShoppingCartComponent } from './components/banner/shopping-cart/shopping-cart.component';
import { TradModule } from './utities/traduction/trad.module';
import { LangComponent } from './components/lang/lang.component';

@NgModule({
  declarations: [
    AppComponent,
    BannerComponent,
    ShoppingCartComponent,
    LangComponent,
    PricePipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    TradModule,
    LoaderModule,
    StoreModule.forRoot({ 
      cards: cardsReducer, 
      filter: filterReducer, 
      pagination: paginationReducer,
      isOpen: eventReducer,
      trad: traductionReducer
    }),
    EffectsModule.forRoot([CardsEffects, FilterEffects, TradEffects])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
